import { Component } from "react";

import steps from '../app/js/data.js';

class Output extends Component {
    
    render() {
        return (
            <>
                <div className="row">
                    <div className="col">
                        <ul>
                            {
                                steps.map((item, index) => {
                                    return (
                                        <li key={index}>{item.id}. {item.title}: {item.content}</li>
                                    )
                                })
                            }
                        </ul>
                    </div>
                </div>
            </>
        )
    }
}
export default Output;