import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css';
import Output from './components/Output';

function App() {
  return (
    <div className='container mt-5'>
      <Output/>
    </div>
  );
}

export default App;
